<?php

use App\Http\Controllers\TicketController;
use Laravel\Lumen\Routing\Router;


Route::post('/api/ticket', [TicketController::class, 'store']);