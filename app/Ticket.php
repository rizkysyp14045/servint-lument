<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'ticket';
    protected $fillable = [
        'user_id', 'detail_kendala', 'category','bearer_token'
    ];
}
