<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bearerToken = $request->header('Authorization');
        $token = str_replace('Bearer ', '', $bearerToken);

        $data = $request->only(['user_id', 'detail_kendala', 'category']);
        $data['bearer_token'] = $token;
        try {
        
            $client = new Client();
            $response = $client->post(env('SERVINT_API') . 'ticket', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => $data,
            ]);
            
            if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
                return $response->getBody();
            }
        } catch (\Exception $e) {

            Ticket::create($data);

            return response()->json(['message' => 'Data stored in SQLite due to API failure',], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */

    
     public function sync()
    {
        $tickets = Ticket::all();

        if ($tickets->isEmpty()) {
            return response()->json(['message' => 'No data to sync.'], 200);
        }

        $client = new Client();

        try {

        foreach ($tickets as $ticket) {
            $bearerToken = $ticket->bearer_token;
            
            $response = $client->request('POST', env('SERVINT_API') . "ticket", [
                'headers' => [
                    'Authorization' => 'Bearer ' . $bearerToken,
                ],
                'json' => $ticket->toArray(),
            ]);

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
                Ticket::where('id', $ticket->id)->delete();
                dd($response->getBody(),'syncup success');
            }
        }
        } catch (\Exception $e) {
            dd($e,"Syncup Gagal");
        }
    }
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
