<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\TicketController;

class SyncTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync gak sih';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'http://localhost:8000');

            if ($response->getStatusCode() == 200) {
                app(TicketController::class)->sync();
                $this->info('Sync successful!');
            } else {
                $this->error('Failed to sync tickets');
            }
        } catch (\Exception $e) {
            $this->error('Error occurred while syncing tickets: ' . $e->getMessage());
        }
    }
}
